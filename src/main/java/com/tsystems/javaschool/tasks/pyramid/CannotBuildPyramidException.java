package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    CannotBuildPyramidException (){
        System.out.println("The pyramid can not be built");
    }
}
