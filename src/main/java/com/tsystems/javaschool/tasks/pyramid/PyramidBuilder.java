package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int countOfLines = checkingPossibilityOfBuildingPyramid(inputNumbers);
        int countOfColumns = countOfLines * 2 - 1;
        int[][] pyramid = new int[0][0];
        if (countOfLines == 0) {
            throw new CannotBuildPyramidException();
        } else {
            List<Integer> numbers = new ArrayList<>(inputNumbers);
            pyramid = new int[countOfLines][countOfColumns];
            Collections.sort(numbers);
            pyramid = constructionThePyramid(numbers, countOfLines, countOfColumns);
        }
        return pyramid;
    }

    private int checkingPossibilityOfBuildingPyramid(List<Integer> inputNumbers){
        int result = 0;
        int amountOfNumbers = inputNumbers.size();
        Set helpSet = new HashSet(inputNumbers);
        if(helpSet.size()==amountOfNumbers) {
            if (!inputNumbers.contains(null)) {
                if (amountOfNumbers <= Integer.MAX_VALUE) {
                    for (int i = 1; i <= amountOfNumbers; i++) {
                        amountOfNumbers = amountOfNumbers - i;
                        if (amountOfNumbers == 0) {
                            result = i;
                        }
                    }
                }
            }
        }
        return result;
    }

    private int[][] constructionThePyramid(List<Integer> inputNumbers, int countOfLines, int countOfColumns){
        int[][] pyramid= new int[countOfLines][countOfColumns];
        int j=0;
        for(int i=0;i<countOfLines;i++){
            j=(countOfColumns-1)/2-i;
            while (j<=(countOfColumns-1)/2+i){
                pyramid[i][j]=inputNumbers.get(0);
                inputNumbers.remove(0);
                j=j+2;
            }
        }
        return pyramid;
    }

}
