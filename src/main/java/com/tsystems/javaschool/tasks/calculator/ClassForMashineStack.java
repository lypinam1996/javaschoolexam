package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.Stack;

public class ClassForMashineStack {

    private String expresion;

    public ClassForMashineStack(String exp) {
        expresion = exp;
    }

    public String Counting() {
        Helper helper = new Helper();
        LinkedList<String> listForCheck = new LinkedList<String>();
        Parser p = new Parser(expresion);
        listForCheck = p.check();
        String res = "";
        if (listForCheck.isEmpty()) {
            res=null;
        }
        else {
            LinkedList<String> listForOPN = new LinkedList<String>();
            listForOPN = p.getExpression(listForCheck);
            if (listForOPN.isEmpty()){
                res=null;
            }
            else{
                String result = "";
                Stack<String> temp = new Stack<String>();
                for (int i = 0; i < listForOPN.size(); i++) {
                    if (helper.isNumber2(listForOPN.get(i))) {
                        temp.push(listForOPN.get(i));
                    }
                    else {
                        if (helper.myMap().containsKey(listForOPN.get(i))) {
                            result = helper.myMap().get(listForOPN.get(i)).solveForTwoArgument(temp.pop(), temp.pop());
                            if(result.equals("")){
                                res=null;
                            }
                            temp.push(result);
                        }
                    }
                }
                res = temp.pop();
            }
        }
        return res;
    }
}
