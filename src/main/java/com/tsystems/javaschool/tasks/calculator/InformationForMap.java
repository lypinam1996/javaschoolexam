package com.tsystems.javaschool.tasks.calculator;

public class InformationForMap {
    private String myexpression;
    private final String space = " ";
    private final String plus = "+";
    private final String minus = "-";
    private final String mult = "*";
    private final String divis = "/";
    private final String leftBkt = "(";
    private final String rightBkt = ")";

    public InformationForMap(String expression){
        myexpression=expression;
    }

    public int getPriority(){
        if ((myexpression.equals(leftBkt)) || (myexpression.equals(rightBkt)) ){
            return 1;
        }
        else
        {
            if ((myexpression.equals(plus)) || (myexpression.equals(minus))){
                return 2;
            }
            else{
                if ((myexpression.equals(mult)) || (myexpression.equals(divis))){
                    return 3;
                }
                else
                {
                    return 4;
                }
            }
        }
    }

    private String solveForPlus(String arg2, String  arg1){
        Double solve2 = Double.parseDouble(arg2);
        String result ="";
        double res=0;
        if (arg1.substring(0,1).equals(minus)){
            Double solve = Double.parseDouble(arg1.substring(1,arg1.length()));
            res = solve2-solve;
            result=res+"";
        }
        else {
            Double solve = Double.parseDouble(arg1);
            result = new Double(solve + solve2).toString();
        }
        return result;
    }

    private String solveForMinus(String arg2, String  arg1){
        Double solve2 = Double.parseDouble(arg2);
        String result ="";
        if (arg1.substring(0,1).equals(minus)){
            Double solve = Double.parseDouble(arg1.substring(1,arg1.length()));
            Double solve1 = Double.parseDouble(arg2);
            result=minus+new Double(solve+solve1).toString();
        }
        else {
            Double solve = Double.parseDouble(arg1);
            result = new Double(solve - solve2).toString();
        }
        return result;
    }

    private String solveForMult(String arg2, String  arg1){
        String result ="";
        if ((arg1.substring(0,1).equals(minus)) && (arg2.substring(0,1).equals(minus))){
            Double solve = Double.parseDouble(arg1.substring(1,arg1.length()));
            Double solve4 = Double.parseDouble(arg2.substring(1,arg2.length()));
            result = new Double(solve4*solve).toString();
        }
        else{
            if (arg1.substring(0,1).equals(minus)){
                Double solve = Double.parseDouble(arg1.substring(1,arg1.length()));
                Double solve4 = Double.parseDouble(arg2);
                result = minus+new Double(solve4*solve).toString();
            }
            else {
                if (arg2.substring(0, 1).equals(minus)) {
                    Double solve = Double.parseDouble(arg2.substring(1, arg1.length()));
                    Double solve4 = Double.parseDouble(arg1);
                    result = minus + new Double(solve4 * solve).toString();
                } else {
                    Double solve = Double.parseDouble(arg2);
                    Double solve4 = Double.parseDouble(arg1);
                    result = new Double(solve4 * solve).toString();;
                }
            }
        }
        return result;
    }

    private String solveForDiv(String arg2, String  arg1){
        String result ="";
        if(!arg2.equals("0.0")) {
            double res = 0;
            if ((arg1.substring(0, 1).equals(minus)) && (arg2.substring(0, 1).equals(minus))) {
                Double solve = Double.parseDouble(arg1.substring(1, arg1.length()));
                Double solve4 = Double.parseDouble(arg2.substring(1, arg2.length()));
                result = new Double(solve / solve4).toString();
            } else {
                if ((arg1.substring(0, 1).equals(minus))) {
                    Double solve = Double.parseDouble(arg1.substring(1, arg1.length()));
                    Double solve4 = Double.parseDouble(arg2);
                    result = minus + new Double(solve / solve4);
                }
                else {
                    if ((arg2.substring(0, 1).equals(minus))) {
                        Double solve4 = Double.parseDouble(arg2.substring(1, arg1.length()));
                        Double solve = Double.parseDouble(arg1);
                        result = minus +new Double(solve / solve4);
                    } else {
                        Double solve = Double.parseDouble(arg2);
                        Double solve4 = Double.parseDouble(arg1);
                        result = new Double(solve4 / solve).toString();
                    }
                }
            }
        }
    return result;
    }

    public String solveForTwoArgument(String arg2, String  arg1){
        String result ="";
        if (myexpression.equals(plus)){
            result=solveForPlus(arg2,arg1);
        }
        else{
            if (myexpression.equals(minus)){
                result=solveForMinus(arg2,arg1);
            }
        }
        if (myexpression.equals(mult)){
            result=solveForMult(arg2,arg1);
        }
        else{
            if (myexpression.equals(divis)){
                result=solveForDiv(arg2,arg1);
            }
        }
        return result;
    }
}