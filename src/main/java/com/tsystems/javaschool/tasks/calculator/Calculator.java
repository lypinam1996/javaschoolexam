package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        Helper helper = new Helper();
        String res = "";
        if(statement==null){
            res=null;
        }
        else {
            LinkedList<String> listForCheck = new LinkedList<String>();
            Parser p = new Parser(statement);
            listForCheck = p.check();

            if (listForCheck.isEmpty()) {
                res = null;
            } else {
                LinkedList<String> listForOPN = new LinkedList<String>();
                listForOPN = p.getExpression(listForCheck);
                if (listForOPN.isEmpty()) {
                    res = null;
                } else {
                    String result = "";
                    Stack<String> temp = new Stack<String>();
                    for (int i = 0; i < listForOPN.size(); i++) {
                        if (helper.isNumber2(listForOPN.get(i))) {
                            temp.push(listForOPN.get(i));
                        } else {
                            if (helper.myMap().containsKey(listForOPN.get(i))) {
                                result = helper.myMap().get(listForOPN.get(i)).solveForTwoArgument(temp.pop(), temp.pop());
                                if(result.equals("")){
                                    temp.push(null);
                                }
                                else {
                                    temp.push(result);
                                }
                            }
                        }
                    }
                    res = temp.pop();
                }
            }
        }
        String result=roundingResult(res);
        return result;
    }

    private String roundingResult(String res){
        String result ="";
        if (res==null){
            result=null;
        }
        else {
            double tempDoubleRes = Double.valueOf(res);
            int tempIntRes = (int) tempDoubleRes;
            if (tempDoubleRes == tempIntRes) {
                result = String.valueOf(tempIntRes);
            } else {
                int pow = 10;
                for (int i = 1; i < 4; i++)
                    pow *= 10;
                double tmp = tempDoubleRes * pow;
                tempDoubleRes= (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;

                result=String.valueOf(tempDoubleRes);
            }
        }
        return result;
    }
}
