package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.Stack;

public class Parser {

    private String expression;
    private final String plus = "+";
    private final String minus = "-";
    private final String mult = "*";
    private final String divis = "/";
    private final String leftBkt = "(";
    private final String rightBkt = ")";

    public Parser(String expression1) {
        expression = expression1;
    }

    public int turnIntoNumber(String str){
        int res=0;
        if (isNumber(str)){
            res=1;
        }
        else{
            if (str.equals(plus)){
                res=2;
            }
            else {
                if (str.equals(minus)){
                    res= 3;
                }
                else {
                    if (str.equals(mult)){
                        res= 4;
                    }
                    else {
                        if (str.equals(divis)) {
                            res = 5;
                        } else {

                            if (str.equals(leftBkt)) {
                                res = 6;
                            } else {
                                if (str.equals(rightBkt)) {
                                    res = 7;

                                }
                            }
                        }
                    }
                }
            }
        }
        return res;
    }

    public boolean isNumber(String num) {
        try {
            Double.parseDouble(num);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public String nextExpression(int i) {
        String result = "";
        boolean ok = true;
        int j = i;
        while (j < expression.length() && ok) {
            String str = expression.substring(j, j + 1);
            if (isNumber(str)) {
                result = result + str;
                j++;
            } else {
                if (str.equals(".")) {
                    if (result.contains(".")) {
                        result = "";
                        ok = false;
                    } else {
                        result = result + str;
                        j++;
                    }
                } else {
                    if (result.isEmpty() && (str.equals(minus) || str.equals(mult) || str.equals(plus) ||
                            str.equals(divis) || str.equals(rightBkt) || str.equals(leftBkt))) {
                        result = str;
                    }
                    ok = false;
                }
            }
        }
        return result;
    }

    private void ifNextNumber(LinkedList<String> finalList,String number){
        if (!finalList.isEmpty()) {
            String k = finalList.getLast();
            if (finalList.size() >= 2) {
                if ((k.equals(minus)) && (finalList.get(finalList.size() - 2).equals(mult)) ||
                        (k.equals(minus)) && (finalList.get(finalList.size() - 2).equals(divis)) ||
                        (k.equals(minus)) && (finalList.get(finalList.size() - 2).equals(leftBkt))) {
                    finalList.remove(finalList.getLast());
                    finalList.addLast(minus + number);
                } else {
                    if (k == rightBkt) {
                        finalList.add(mult);
                        finalList.add(number);
                    } else {
                        finalList.addLast(number);
                    }
                }
            }
            if (finalList.size() == 1) {
                if (k == minus) {
                    finalList.remove(finalList.getLast());
                    finalList.addLast(minus + number);
                }
                else{
                    if(k==leftBkt){
                        finalList.addLast(number);
                    }
                    else {
                        finalList.clear();
                    }
                }
            }
        }
        else{
            finalList.addLast(number);
        }
    }

    private void ifNextBktLeft(LinkedList<String> finalList,String number){
        if (!finalList.isEmpty()) {
            String k = finalList.getLast();
            if (isNumber(k)) {
                finalList.add(mult);
                finalList.add(leftBkt);
            }
            else {
                finalList.add(leftBkt);
            }
        }
        else {
            finalList.add(leftBkt);
        }
    }

    private void ifNextBktRight(LinkedList<String> finalList,String number) {
        if (finalList.size() >= 3) {
            if (finalList.get(finalList.size() - 2).equals(leftBkt)) {
                System.out.println("Error in expression! The number is in brackets!");
                finalList.clear();
            } else {
                if (!finalList.isEmpty()) {
                    String k = finalList.getLast();
                    if ((k.equals(plus)) || (k.equals(minus)) ||
                            (k.equals(divis)) || (k.equals(mult)) ||
                            (k.equals(leftBkt))) {
                        System.out.println("Error in expression! Two operations are followed by each other!");
                        finalList.clear();
                    } else {
                        finalList.add(rightBkt);
                    }
                } else {
                    System.out.println("Error in expression! The closing bracket is at the beginning of the sentence!");
                    finalList.clear();
                }
            }
        }
    }

    private void ifNextOperation(LinkedList<String> finalList,String number){
        if (!finalList.isEmpty()) {
            String k = finalList.getLast();
            if (!number.equals(minus)) {
                if ((k.equals(plus)) || (k.equals(minus)) || (k.equals(divis)) || (k.equals(mult)) || (k.equals(leftBkt))) {
                    System.out.println("Error in expression! Two operations are followed by each other");
                    finalList.clear();
                }
                else {
                    finalList.add(number);
                }
            } else {
                if ((k.equals(plus)) || (k.equals(minus)) || (k.equals(divis)) || (k.equals(mult))) {
                    System.out.println("Error in expression! Two operations are followed by each other");
                    finalList.clear();
                } else {
                    if (k.equals(leftBkt)) {
                        finalList.add("0");
                        finalList.add(number);
                    } else {
                        finalList.add(number);
                    }
                }
            }
        }
        else {
            if (!number.equals(minus)) {
                System.out.println("Знак операции в начале предложения! Будьте внимательны!");
                finalList.clear();
            } else {
                finalList.add(minus);
            }
        }
    }

    public LinkedList<String> check(){
        LinkedList<String> finalList = new LinkedList<String>();
        int i = 0;
        boolean ok = true;
        while (i < expression.length() && ok) {
            String number = nextExpression(i);
            if (number.equals("")) {
                finalList.clear();
                ok=false;
            } else {
                int count = turnIntoNumber(number);
                switch (count) {
                    case 1: {
                        ifNextNumber(finalList, number);
                        if (finalList.isEmpty()) {
                            ok = false;
                        } else {
                            int y = number.length();
                            i = i + y;
                        }
                        break;
                    }
                    case 2: {
                        ifNextOperation(finalList, number);
                        if (finalList.isEmpty()) {
                            ok = false;
                        } else {
                            i++;
                        }
                        break;
                    }
                    case 3: {
                        ifNextOperation(finalList, number);
                        if (finalList.isEmpty()) {
                            ok = false;
                        } else {
                            i++;
                        }
                        break;
                    }
                    case 4: {
                        ifNextOperation(finalList, number);
                        if (finalList.isEmpty()) {
                            ok = false;
                        } else {
                            i++;
                        }
                        break;
                    }
                    case 5: {
                        ifNextOperation(finalList, number);
                        if (finalList.isEmpty()) {
                            ok = false;
                        } else {
                            i++;
                        }
                        break;
                    }
                    case 6: {
                        ifNextBktLeft(finalList, number);
                        if (finalList.isEmpty()) {
                            ok = false;
                        } else {
                            i++;
                        }
                        break;
                    }
                    case 7: {
                        ifNextBktRight(finalList, number);
                        if (finalList.isEmpty()) {
                            ok = false;
                        } else {
                            i++;
                        }
                        break;
                    }

                    default: {
                        System.out.println("Invalid characters entered!");
                        ok = false;
                        finalList.clear();
                    }
                }
            }
        }
        return finalList;
    }

    public LinkedList<String> getExpression(LinkedList<String> finalList) {
        LinkedList<String> finalqueue = new LinkedList<String>();
        Helper helper = new Helper();
        Stack<String> operStack = new Stack<String>();
        int countOpen = 0;
        int countClose =0;

        for (int i = 0; i < finalList.size(); i++) {
            if (isNumber(finalList.get(i))) {
                finalqueue.add(finalList.get(i));
            }
            else {
                if (helper.myMap().containsKey(finalList.get(i))) {
                    if (finalList.get(i) == leftBkt) {
                        countOpen++;
                        operStack.push(finalList.get(i));
                    }
                    else {
                        if (finalList.get(i) == rightBkt) {
                            countClose++;
                            if(operStack.search(leftBkt)!=-1){
                                if (operStack.size() > 0) {
                                    if (operStack.peek() != leftBkt) {
                                        String s = operStack.peek();
                                        while (operStack.pop() != leftBkt) {
                                            finalqueue.add(s);
                                            s = operStack.peek();
                                        }
                                        countClose--;
                                        countOpen--;
                                    } else {
                                        if (operStack.peek() == leftBkt) {
                                            operStack.pop();
                                            countClose--;
                                            countOpen--;
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            if (operStack.size() > 0) {
                                if (helper.myMap().get(finalList.get(i)).getPriority() <= helper.myMap().get(operStack.peek()).getPriority()) {
                                    String l = operStack.pop();
                                    finalqueue.add(l);
                                    operStack.push(finalList.get(i));
                                }
                                else {
                                    operStack.push(finalList.get(i));
                                }
                            }
                            else {
                                operStack.push(finalList.get(i));
                            }
                        }
                    }
                }
            }
        }

        while (operStack.size() > 0) {
            finalqueue.add(operStack.pop());
        }
        if((countClose)!=(countOpen)){
            System.out.println("Unequal number of brackets! ");
            finalqueue.clear();
        }
        return finalqueue;
    }
}


