package com.tsystems.javaschool.tasks.calculator;

import java.util.HashMap;

public class Helper {

    public boolean isNumber2(String num) {
        boolean ok = true;
        if ((num.substring(0, 1) == "-") || (num.substring(0, 1) == "+")) {
            String res = num.substring(1, num.length());
            try {
                Double.parseDouble(res);
                ok = true;
            } catch (NumberFormatException e) {
                ok = false;
            }
        } else {
            try {
                Double.parseDouble(num);
                ok = true;
            } catch (NumberFormatException e) {
                ok = false;
            }

        }
        return ok;
    }

    public HashMap<String, InformationForMap> myMap() {
        HashMap<String, InformationForMap> mapForOperators = new HashMap<String, InformationForMap>();
        mapForOperators.put("+", new InformationForMap("+"));
        mapForOperators.put("-", new InformationForMap("-"));
        mapForOperators.put("(", new InformationForMap("("));
        mapForOperators.put(")", new InformationForMap(")"));
        mapForOperators.put("*", new InformationForMap("*"));
        mapForOperators.put("/", new InformationForMap("/"));
        return mapForOperators;
    }
}
