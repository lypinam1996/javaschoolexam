package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        boolean result = true;
        if(x!=null && y!=null) {
            if (x.size() > y.size()) {
                result = false;
            } else {
                if (y.containsAll(x)) {
                    ArrayList<Integer> indexes = new ArrayList<Integer>();
                    for (int i = 0; i < x.size(); i++) {
                        indexes.add(y.indexOf(x.get(i)));
                    }
                    int i = 0;
                    while (i < indexes.size() - 1 && result) {
                        if (indexes.get(i) > indexes.get(i + 1)) {
                            result = false;
                        } else {
                            i++;
                        }
                    }
                } else {
                    result = false;
                }
            }
        }
        else{
            throw new IllegalArgumentException();
        }
        return result;
    }
}
